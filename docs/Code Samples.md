# Code samples

This file provides code snippets in Abakus and their equivalents in C++ and/or Python for future refrence. This is not an exhaustive definition of the language. Further, these examples are somewhat basic and may in part be expandeable with non-mandatory structures.

## Variable definition and initialisation

### Abakus

Code: "STORE 5 INTO x AS INTEGER"

Structure: "STORE \<value or variable\> \<targeting auxiliary\> \<target variable\> \<cast\>"
Variables are type-agnostic in general, or rather they store both the value and the values type. They may be cast into specific types as seen above, using either predefined or custom types. If no type is specifies (no cast structure), then either the type of the last value is used, or if the variable is initialised for the first time, the interpreter guesses.

### Python

```Python
x = 5
```

### C++

```C++
int x = 5;
```

## Casting a variable to another type

### Abakus

Code: "CAST x AS INTEGER"

Structure: "CAST \<target variable\> \<cast\>"
The CAST command is technically without meaning here, as the actual casting happens based on the cast structure at the end of the line. It is neccessary here for conformity, but may be replaced with another command that also does not represent an operation as such. (aka "EXEC", "NOP").

### Python

```Python
x = int(x)
```

### C++

```C++
int x = (int) x;
```

Note: This is technically not the same variable, but it's as close as we're going to get.

## Printing to the Console

### Abakus

Code:"PRINT X"

Structure: "PRINT \<target variable\>"
Prints the value of the variable to the console in a format dictated by the type it currently has. In case a type has no print-format set, a standard format based on the STRING type is used.

### Python

```Python
print x
```

### C++

```C++
std::cout << x << std::endl;
```

## Function execution without result storage

### Abakus

Code: "EXEC f(x)"

Structure: "EXEC <function identifier>(<list of arguments>)"

Note that EXEC has no inherent functionality here, it could be replaced with "NOP" or "CAST", see the section on casting. Note that the list of arguments may be empty.

### Python

```Python
f(x)
```

### C++

```C++
f(x);
```

## Function execution with result storage

### Abakus

Code: "STORE f(x) INTO x" 

Structure: "STORE \<function identifier\>(\<list of arguments\>) \<targeting auxiliary\> \<target var\>"

Like a normal "STORE", but using the result of a function execution as the value. Note that this version does not include a cast.

### Python

```Python
x = f(x)
```

### C++

```C++
x = f(x);
```

## Function definition

### Abakus

Code: "FUNC f(x: ANY)"

Structure:"FUNC \<function identifier\>(\<list of \<argument name: expected type\>\>)"
Note: This starts a new codeblock, which ends with \<TBD\>. The whole block forms a new scope and the body of the function. Note that the list of arguments may be empty, and that the non-type "ANY" may be used in place of a specific type.

### Python

```Python
def f(x):
```

### C++

```C++
void f(any x){
```

## Importing

### Abakus

Code: "IMPORT math"

Structure: "IMPORT \<module name\>"

Provides support for modules written in C++ or in Abakus. Alternatively, importing under an alias ("IMPORT math PSEUDO m") is possible, as is importing specific functions or submodules ("IMPORT pow FROM math"). \<TBD: Paths\>

### Python

```Python
import math
```

### C++

```C++
#include <math>
```
