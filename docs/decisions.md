# Decisions

The purpose of this file is to cronicle and explain the major decisions made during the development of Abakus, so that they may be referred to later for refrence and/or change. It will be appended to as the project evolves.

## Architecture

### Interpretation vs Compilation

The language is interpreted rather than compiled in order to simplify the architecture neccessary, and in order to more easily allow for the expansion of any future GUI interpreter with learning tools.

### Distinct components

The interpreters components, namely the Lexer, the Parser and the Context Handler are split and individually useable so that they can be used as teaching tools for their own purposes themselves, so that they can be more easily tested independently, and so that improvements can be made to one with minimal relevance to the others. 

## Semantis and Syntax

### Imperative

Most lines in the language use a format based on natural language imperative statements, in the hope that these will be simple and intuitively understood as commands.

## Language Design

### Rationale

Abakus is primarily intended as a learning tool. Programming beginners face a major hurdle in needing to learn both programming as a skill and a programming language at the same time. This hurdle is not only quite harsh, it is also, assuming one takes on projects proportional to ones current skill, the highest hurdle most programmers will ever face - later on, you will only ever need to learn a new programming language or new skills at any one time. Even when both are neccessary for a given project, it's almost always possible to space them out in order to make learnign easier. Abakus aims to allow for a beginner to do much the same by providing a programming langauge that is so simple and intuitively understood that it need not be learned in the traditional sense. With this, users will be able to learn programming as a skill and then afterwards learn a more classic programming language with the backdrop of already knowing how to program, significantly reducing the difficulty of the task at hand.

### Name

Abakus is mainly a teaching tool, and one that is not intended to be used once the learners skills are established. As such, it mirrors and abacus. The usage of the german spelling ("k" instead of "c") is to differentiate the words.
