#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#pragma once

int main(int, char**);
bool isInputOk(char*);
std::vector<std::string> lex(char*);
bool validCommand(std::string);
bool isTokenListOkay(std::vector<std::string>);