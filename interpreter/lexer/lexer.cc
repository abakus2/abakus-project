#include "lexer.h"

bool validCommand(std::string com){
	/*
	Checks if the supplied string is a command valid in Abakus

	- com: command to check
	*/
	//Also sorts out comments simply by virtue of the fact that they begin with a spcific set of characters that invalidate the command
	std::vector<std::string> legal_commands{"NOP", "STORE", "CAST", "PRINT", "EXEC", "FUNC", "IMPORT"};//To be expanded //Maybe handle FUNC and IMPORT otherwise?
	for(int i = 0; i < legal_commands.size(); i++){
		if(com == legal_commands[i]){
			return true;
		}
	}
	return false;
}

bool isTokenListOkay(std::vector<std::string> tokens){
	std::string com = tokens[0];
	if(com == std::string("STORE")){
		return tokens.size() == 4;
	}
	if(com == std::string("PRINT")){
		return tokens.size() == 2;
	}
	return true;
}

std::vector<std::string> lex(char* input){
	/*
	Applies the Lexer to the supplied input

	- input: char* representing a line of the program 
	*/
	//Should probably call isInputOk here, once that actually does something
	//Split string into tokens
	std::vector<std::string> tokens;
	std::istringstream iss(input);
	std::string s;
	while(getline(iss, s, ' ')){
		tokens.push_back(s);
	}
	//Now, tokes contains all ' '-delimited tokens in the line. We need to check that the line is valid now.
	if(validCommand(tokens[0]) && isTokenListOkay(tokens)){
		//TODO: Reunify equations and function calls
		return tokens;
	}
	//TODO - check if line is comment
	//TODO - Further checks
	return std::vector<std::string>{std::string("-1")};
}

bool isInputOk(char* input){
	//Checks input for the lexer in those senses that aren't relevant for the interpreter
	bool result = true;
	
	return result;
}

/*int main(int argc, char* argv[]){
	std::cout << "NOTE: This is just the Abakus Lexer, not the Parser or anything further." << std::endl;
	if(argc != 2 || !isInputOk(argv[1])){
		//Malformed Input
		return 1;
	}
	//Good input, continue
	std::vector<std::string> result = lex(argv[1]);
	//TODO: Check for indication of bad validity
	for(int i = 0; i < result.size(); i++){
		std::cout << i << ": " << result[i] << std::endl;
	}
}*/