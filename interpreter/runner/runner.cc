#include "runner.h"

context run_line(std::vector<std::string> tree, context con){
    //TODO: Resolve Equations
    //TODO: Execute functions
    if(tree[0] == std::string("PRINT")){
        std::cout << tree[1] << std::endl;
    }

    if(tree[0] == std::string("STORE")){
        auto val = tree[1];
        auto var = tree[3];
        //TODO: Recursive search in higher contexts
        if(val[0] == '"'){
            val.erase(0, 1);
            if(val[val.size() -1] == '"'){
                val.erase(val.size()-1);
            }
        }
        con.set_var_val(var, val);
        if(con.get_var_type(var) == std::string("")){
            con.set_var_type(var, std::string("string"));
        }
    }

    //TODO: Handle other commands

    return con;
}