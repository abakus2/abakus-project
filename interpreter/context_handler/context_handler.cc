#include "context_handler.h"

void context::create_var(std::string name){
	/*
	Create new Variable in this context

	- name: Name of the new variable
	*/
	auto var = context_variable();
	var.type = std::string("any");
	var.val = std::string("");
	current_vars[name] = var;
}

void context::create_var(std::string name, std::string type, std::string val){
	/*
	Create new Variable in this context

	- name: Name of the new variable
	- type: Type of the new Variable
	- val: Value of the new Variable
	*/
	auto var = context_variable();
	var.type = type;
	var.val = val;
	current_vars[name] = var;
}

void context::set_var_type(std::string name, std::string type){
	/*
	Set Type for specific Variable

	- name: Name of Variable for which to set type
	- type: Type to which to set the Variable
	*/
	current_vars[name].type = type;
}

void context::set_var_val(std::string name, std::string val){
	/*
	Set Value for specific Variable

	- name: Name of Variable for which to set type
	- val: Value to which to set the Variable
	*/
	current_vars[name].val = val;
}

std::string context::get_var_type(std::string name){
	/*
	Get the Type of a specific Variable
	
	- name: Name identifying the relevant Variable
	*/
	return current_vars[name].type;
}

std::string context::get_var_val(std::string name){
	/*
	Get the Value of a specific Variable
	
	- name: Name identifying the relevant Variable
	*/
	return current_vars[name].val;
}

void context::print_context(){
	/*
	Prints the specific Context in a specific format
	*/
	for(auto it = current_vars.begin(); it != current_vars.end(); ++it){
		std::cout << it->first << ": " << it->second.val << ", " << it->second.type << std::endl;
	}
	//TODO: Repeat for Functions
}

std::vector<std::string> context::add_variables_from_context(std::vector<std::string> tree){
	//Remove reserved words
	std::vector<std::string> reserved_words{"NOP", "STORE", "CAST", "PRINT", "EXEC", "FUNC", "IMPORT", "AS", "INTO"}; //Reserved Words can't be Variables
	for(int i = 1; i < tree.size(); i++){
		bool is_reserved = false;
		for(int j = 0; j < reserved_words.size(); j++){
			if(tree[i] == reserved_words[j]){
				is_reserved = true;
			}
		}
		if(is_reserved){
			continue;
		}
		//TODO: remove functions
		//Remove Tokens whose predecessor shows they are targets
		if(tree[i-1] == std::string("INTO")){
			continue;
		}
		//Remove Tokens starting with numbers
		bool legal_variable_name = true;
		for(int j = 0; j < 10; j++){
			if(tree[i].rfind(std::to_string(j), 0) == 0){
				legal_variable_name = false;
			}
		}
		if(tree[i][0] == '"'){
			legal_variable_name = false;
		}
		if(!legal_variable_name){
			continue;
		}
		//Only the variables we want to replace remain
		try
		{
			tree[i] = this->get_var_val(tree[i]);
		}
		catch(const std::exception& e)
		{
			std::cout << "Couldn't fill argument: " << tree[i] << std::endl;
			std::cout << e.what() << std::endl;
		}
		//TODO: Replace Variables in equations and function calls
	}
	return tree;
}
