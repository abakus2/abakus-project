#pragma once
#include <map>
#include <iostream>
#include <vector>
#include <string>

struct context_variable{
	std::string type;
	std::string val;
};

class context{
private:
	std::map<std::string, context_variable> current_vars;
	std::map<std::string, int> current_functions; //Placeholder for when I work out CSTs.
public:
	void print_context();
	context_variable get_var(std::string);
	void create_var(std::string);
	void create_var(std::string, std::string, std::string);
	void set_var_type(std::string, std::string);
	void set_var_val(std::string, std::string);
	std::string get_var_type(std::string);
	std::string get_var_val(std::string);
	//TODO: Access for functions

	std::vector<std::string> add_variables_from_context(std::vector<std::string>);
};