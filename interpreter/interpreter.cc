#include "interpreter.h"
#include "./lexer/lexer.h"
#include "./context_handler/context_handler.h"
#include "./runner/runner.h"

int main(int argc, char* argv[]){
	//Intialise standard context
	context root_context;

	if(argc == 1){ //Line-by-Line mode
		std::cout << "Abakus interpreter V0.01 (q to quit)" << std::endl;
		//TODO: While the input isn't the end token, get line, lex line, parse line, execute, update context, repeat
		std::string line;
		char* c_line = new char[0];
		while(true){
			std::cout << ">> ";
			std::getline(std::cin, line);
			if(line == "q"){
				break;
			}
			c_line = new char[line.length() + 1];
			strcpy(c_line, line.c_str());
			std::cout << std::endl;

			//Splits line into tokens
			auto result = lex(c_line);
			//Contextualises tokens
			auto tree = root_context.add_variables_from_context(result);
			//Run line
			root_context = run_line(tree, root_context);
			delete [] c_line;
		}
	}else{ // Might be file mode
		//TODO: Read file line for line, then excute as above
		//Await input before closing?
	}
}